#!/bin/sh

if [ $# -ne 1 ]
  then
    echo "usage pass <password length>"
    exit 1
fi

openssl rand -base64 $1 |md5 |cut -c-$1